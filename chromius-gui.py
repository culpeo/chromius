#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  chromius-gui.py
#
#  Copyright 2021 Robert Kluj <culpeo@dismail.de>
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

#  GUI imports
import tkinter as tk
from tkinter import messagebox, filedialog

#  Chromius imports
from os import chdir
from pathlib import Path
import xml.etree.ElementTree
import re
import pandas

#  Chromius core functionality
def main(file_name, user_dir):

    def set_output_file_names(file_name):
        name_user = file_name
        output_files = {}
        output_files['rawdata'] = name_user
        output_files['metadata'] = name_user.strip('.csv') + '_metadata.txt'
        return output_files

    def get_paths(user_dir):
        # Checks with is_file() or exists() do not work. Thus, paths of
        # individual samples meta data files are read precautionary.
        chdir(user_dir)
        paths_metadata = Path().glob('*.xml')
        paths_metadata_fallback = (path for path in Path().glob('*/*.xml'))
        paths_rawdata = (path for path in Path().glob('*/*.xy')
                        if 'BPC' in path.name
                        or 'EIC' in path.name
                        or 'TIC' in path.name
                        )
        return paths_metadata, paths_rawdata, paths_metadata_fallback

    def parse_xml(xml_file):
        xml_tree = xml.etree.ElementTree.parse(xml_file)
        xml_parsed = xml_tree.getroot()
        return xml_parsed

    def get_run_info(xml_parsed, metadata):
        run_info = xml_parsed.find('SampleTableHeader').attrib
        run = metadata['run']
        run['Date'] = run_info['UpdateDate']
        run['HyStar version'] = run_info['HyStarVersion']
        run_info = xml_parsed.find('Sample').attrib
        run['Method'] = run_info['SuperMethod']
        return metadata

    def get_sample_info(xml_parsed, sample_regex, metadata):
        for item in xml_parsed.iter('Sample'):
            sample_mobj = sample_regex.search(item.attrib['ResultDatafile'])
            sample_id = sample_mobj.group(3)
            metadata['samples'][sample_id] = {}
            sample = metadata['samples'][sample_id]
            sample['Name'] = item.attrib['SampleID']
            sample['Position'] = item.attrib['Position']
            sample['Volume (µl)'] = item.attrib['Volume']
        return metadata

    def get_metadata(paths_metadata, paths_metadata_fallback):
        paths = paths_metadata
        paths_fallback = paths_metadata_fallback
        metadata = {'run': {}, 'samples': {}}
        sample_regex = re.compile(r'(.+)(_\w{2}\d_\d{2}_)(\d{5,}\S*)(.d)')

        for index, path in enumerate(paths):
            with path.open() as open_file:
                xml_parsed = parse_xml(open_file)
                if index == 0:
                    metadata = get_run_info(xml_parsed, metadata)
                metadata = get_sample_info(xml_parsed, sample_regex, metadata)

        # Fallback to individual sample meta data files if main file is
        # missing (see get_paths(), line 41).
        dict_filled = bool(metadata['run'])
        if dict_filled is False:
            for index, path in enumerate(paths_fallback):
                with path.open() as open_file:
                    xml_parsed = parse_xml(open_file)
                    if index == 0:
                        metadata = get_run_info(xml_parsed, metadata)
                    metadata = get_sample_info(xml_parsed, sample_regex, metadata)

        return metadata

    def identify_dataset(path, metadata, regex):
        folder = str(path.parents[0])
        filename = str(path.name)
        sample_mobj = regex['sample'].search(folder)
        sample_name = sample_mobj.group(1)
        sample_id = sample_mobj.group(3)
        if sample_id not in metadata['samples']:
            metadata['samples'][sample_id] = {}
            sample = metadata['samples'][sample_id]
            sample['Name'] = sample_name + '_diffchrom'
        chromatogram_mobj = regex['chrom'].search(filename)
        trace = chromatogram_mobj.group()
        return sample_id, metadata, trace

    def read_rawdata(dataset, rawdata, chromatogram, sample_time=None):
        data_split = (datapoint.split() for datapoint in dataset)
        if sample_time:
            rawdata[sample_time] = [float(time)/60 for time, signal in data_split]
        rawdata[chromatogram] = [signal for time, signal in data_split]
        return rawdata

    def get_rawdata(paths_rawdata, metadata):
        paths = paths_rawdata
        meta_loc = metadata
        rawdata = {}
        regex = {
            'sample': re.compile(r'(.+)(_\w{2}\d_\d{2}_)(\d{5,}\S*)(.d)'),
            'chrom': re.compile(r'(\w+\s)(\d+\.\d|\-|\+)')
            }

        for index, path in enumerate(paths):
            sample_id, meta_loc, trace = identify_dataset(path, meta_loc, regex)
            print('File', str(index) + ':', 'Sample', sample_id, '(' + trace + ')')
            chromatogram = sample_id + trace
            rawdata[chromatogram] = []
            sample_time = sample_id + 'time'
            if sample_time not in rawdata.keys():
                rawdata[sample_time] = []
                with path.open() as dataset:
                    rawdata = read_rawdata(dataset, rawdata, chromatogram, sample_time)
            with path.open() as dataset:
                rawdata = read_rawdata(dataset, rawdata, chromatogram)

        return rawdata, meta_loc

    def align_datasets(rawdata):
        # Not very elegant compensation for data sets of different
        # lengths, as these cause an error in one of the subsequent steps.
        lengths_datasets = {key: len(value) for key, value in rawdata.items()}
        length_min = min(lengths_datasets.values())
        for key, value in rawdata.items():
            rawdata[key] = value[:length_min]
        return rawdata

    def write_rawdata(rawdata, output_rawdata):
        df = pandas.DataFrame(rawdata)
        df.to_csv(output_rawdata, index=False)

    def write_metadata(f, metadata):
        f.write('Run details' + '\n' + '-----------' + '\n')
        for key, value in metadata['run'].items():
            f.write(key + ': ' + value + '\n')
        f.write('\n')
        f.write('Sample details' + '\n' + '--------------' + '\n')
        for sample, details in metadata['samples'].items():
            f.write('Sample ID: ' + sample + '\n')
            for item, value in details.items():
                f.write(item + ': ' + value + '\n')
            f.write('\n')
        f.write('\n')

    def write_mappings(f, metadata):
        f.write('Sample mappings' + '\n' + '---------------' + '\n')
        for sample, details in metadata['samples'].items():
            f.write(details['Name'] + ' = str(' + sample + ')' + '\n')
        f.write('\n')

    def write_reports(rawdata, metadata, output_files):
        data_loc = rawdata
        meta_loc = metadata
        output_loc = output_files
        write_rawdata(data_loc, output_loc['rawdata'])
        print('\n' + 'Raw data written to', output_loc['rawdata'] + '.')
        with open(output_loc['metadata'], 'w') as output_file:
            write_metadata(output_file, meta_loc)
            write_mappings(output_file, meta_loc)
        print('Meta data written to', output_loc['metadata'] + '.')

    #  Call functions
    output_files = set_output_file_names(file_name)
    paths_metadata, paths_rawdata, paths_metadata_fallback = get_paths(user_dir)
    metadata = get_metadata(paths_metadata, paths_metadata_fallback)
    rawdata, metadata = get_rawdata(paths_rawdata, metadata)
    rawdata = align_datasets(rawdata)
    write_reports(rawdata, metadata, output_files)

#  Chromius GUI
def retrieve_dir():
    global user_dir
    user_dir = tk.filedialog.askdirectory(initialdir='/', title='Select directory of your data')
    return user_dir

def save_file():
    global file_name
    file_types = [('csv files', '*.csv')]
    file_name = tk.filedialog.asksaveasfilename(
        initialdir=user_dir,
        title='Choose file name for result',
        filetypes=file_types,
        defaultextension=file_types
        )
    return file_name

def process():
    try:
        main(file_name, user_dir)  # Call Chromius
        tk.messagebox.showinfo(
            'I guess, we are done',
            'Everything went smoothly. See you soon!'
            )
    except NameError:
        tk.messagebox.showerror(
            'Rattling noise',
            'That looks not right. Did you choose an input directory and name an output file?'
            )

#  Create GUI
gui = tk.Tk()
gui.title('Chromius 1.2.1')

frame = tk.Frame(gui)
frame.pack()

label = tk.Label(frame, text='Welcome to Chromius!')
label.pack(padx=10, pady=10)

input_button = tk.Button(frame, text='Select input directory', command=retrieve_dir)
input_button.pack(pady=5)

output_button = tk.Button(frame, text='Select directory and name of output', command=save_file)
output_button.pack(padx=10)

submit_button = tk.Button(frame, text='Submit', command=process)
submit_button.pack(pady=15)

quit_button = tk.Button(frame, text='Quit', command=gui.quit)
quit_button.pack(pady=10)

gui.mainloop()
