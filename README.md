## Chromius

Chromius is a small, simple application for reading meta data and raw data of LC-MS analyses from XML and text files. It is written in Python and based on an idea and implementation by Axel Walter. It is thus the result of an extensive revision by me.

### Purpose

The process of data evaluation of LC-MS analyses is not a trivial task. A common approach is to compare the abundance of specific compounds or masses in a sample to a reference. Depending on the functions of the applications provided by the manufacturer, the possibilities for evaluation and presentation may not meet your own expectations. Luckily, these applications often allow the export of data in various formats. Chromius can read data from plain text files and write their content into collective data tables. The data tables can be used for subsequent analysis or visualisation in spreadsheet applications or plots.

### Use

Chromius is preferably used from within an IDE, as it allows easy inspection and customisation of the source code. Chromius is provided with command line interface (CLI) and grapical user interface (GUI). The CLI version expects the Python packages `pathlib`, `xml.etree.ElementTree`, `re` and `pandas` to be available, the GUI version additionally `tkinter`.

The CLI version has to be placed within the same directory as the files and folders to be read. The GUI version can be used from other directories. The chromatogram data is written into a comma-separated file which can be used for visualisation or further analyses.
For further instructions, please refer to the manual. It is included in the archives offered for download.

The latest release version can be found in [releases](https://codeberg.org/culpeo/chromius/releases).

### Further information

It should be mentioned that Chromius makes some assumptions how the input files are organised, since it is adapted to a certain LC-MS system. If input data is basically provided in a comparable form, it should be possible to customise the application to this without major effort.

Chromius is actively used, but currently not further developed. However, its code is kept simple and certainly has room for improvement. Therefore, there is no guarantee and the source code is provided as is. Suggestions and ideas are welcome.
